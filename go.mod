module todo

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/DATA-DOG/godog v0.7.13
	github.com/appleboy/gin-revision-middleware v0.0.0-20160722161421-cc548e45b51a
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/protobuf v1.3.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/matryer/runner v0.0.0-20190427160343-b472a46105b1
	github.com/micro/cli v0.2.0
	github.com/micro/go-micro v1.18.0
	github.com/miekg/dns v1.1.25 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v0.0.0-20190330032615-68dc04aab96a
	gitlab.com/ty-go-poc/core v1.0.5
	golang.org/x/crypto v0.0.0-20191206172530-e9b2fee46413 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20191210023423-ac6580df4449 // indirect
)

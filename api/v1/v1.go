package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"todo/api/v1/tasks"
	"todo/api/v1/users"
)

func InitEndpoints(router *gin.RouterGroup, logger *logrus.Logger) {
	tasks.NewGateWay(router, logger)
	users.NewGateWay(router, logger)
}

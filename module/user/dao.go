package user

import (
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/ty-go-poc/core"
)

var (
	log = logrus.New()
)

func GetAll(db *sqlx.DB) *List {
	res := List{}
	err := db.Select(&res, "SELECT * from users")
	if err != nil {
		log.Error(err)
		return &res
	}
	return &res
}

func Insert(usr *User, db *sqlx.DB) *core.TYPoc {
	if err := usr.IsValid(); err != nil {
		return err
	}
	res, exErr := db.NamedQuery("INSERT INTO users (name, email) VALUES (:name, :email) RETURNING  id", *usr)
	if exErr != nil {
		log.Info(exErr)
		return core.NewDatastoreError("Test.INSERT", "query", exErr.Error())
	}
	defer res.Close()

	var id int64
	if res.Next() {
		if errInsert := res.Scan(&id); errInsert != nil {
			return core.NewDatastoreError("Test.INSERT", "last insert id", errInsert.Error())
		}
	} else {
		return core.NewDatastoreError("Test.INSERT", "last insert id", "No id returned")
	}

	usr.Id = id
	return nil
}

func GetOne(id int64, db *sqlx.DB) *User {
	user := User{}
	if err := db.Get(&user, "SELECT * FROM users where id = $1", id); err != nil {
		log.Error(err.Error())
		return nil
	}
	return &user
}

func CheckID(id int64, db *sqlx.DB) bool {
	user := User{}
	if err := db.Get(&user, "SELECT * FROM users where id = $1", id); err != nil {
		log.Error(err.Error())
		return false
	}
	return user != User{}
}

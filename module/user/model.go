package user

import (
	"encoding/json"
	"gitlab.com/ty-go-poc/core"
	"io"
)

// User table model
type User struct {
	Id    int64  `form:"-" json:"-" sqlParameterName:"id"`
	Name  string `form:"name" json:"name" sqlParameterName:"name"`
	Email string `form:"email" json:"email" sqlParameterName:"email"`
}

// UserList is a shortcut to a list of User
type List []*User

// IsValid check if User object is valid
func (u *User) IsValid() *core.TYPoc {
	if u.Name == "" {
		return core.NewModelError("User.IsValid", "name", "name required")
	}
	if u.Email == "" {
		return core.NewModelError("User.IsValid", "email", "email required")
	}
	return nil
}

// ToJson serializes the bot patch to json.
func (u *User) ToJson() []byte {
	data, err := json.Marshal(u)
	if err != nil {
		return nil
	}

	return data
}

func FromJson(data io.Reader) *User {
	decoder := json.NewDecoder(data)
	var userData User
	err := decoder.Decode(&userData)
	if err != nil {
		return nil
	}

	return &userData
}

// ToJson serializes the bot patch to json.
func (ul *List) ToJson() []byte {
	data, err := json.Marshal(ul)
	if err != nil {
		return nil
	}

	return data
}

func ListFromJson(data io.Reader) *List {
	decoder := json.NewDecoder(data)
	var userList List
	err := decoder.Decode(&userList)
	if err != nil {
		return nil
	}

	return &userList
}

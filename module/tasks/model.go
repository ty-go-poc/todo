package tasks

import (
	"encoding/json"
	"github.com/jmoiron/sqlx"
	"gitlab.com/ty-go-poc/core"
	"io"
	"todo/module/user"
)

const (
	TaskBacklog    = 0
	TaskInProgress = 1
	TaskDone       = 2
)

// Task table model
type Task struct {
	Id         int64     `form:"-" json:"-" db:"id"`
	Resume     string    `form:"resume" json:"resume" db:"resume"`
	Content    string    `form:"content" json:"content" db:"content"`
	ReporterId int64     `form:"reporter_id" json:"reporter_id" db:"reporter_id"`
	WorkerId   int64     `form:"worker_id" json:"worker_id" db:"worker_id"`
	Reporter   user.User `form:"reporter" json:"reporter" db:"-"`
	Worker     user.User `form:"worker" json:"worker" db:"-"`
	Status     int64     `form:"-" json:"-" db:"status"`
	StatusStr  string    `form:"status" json:"status" db:"status"`
}

// List is a shortcut to a list of Task
type List []*Task

// IsValid check if Task object is valid
func (t *Task) IsValid(db *sqlx.DB) *core.TYPoc {
	if t.Resume == "" {
		return core.NewModelError("Task.IsValid", "resume", "resume required")
	}

	if t.ReporterId != 0 && !user.CheckID(t.ReporterId, db) {
		return core.NewModelError("Task.IsValid", "reporter_id", "invalid reporter")
	}
	if t.WorkerId != 0 && !user.CheckID(t.WorkerId, db) {
		return core.NewModelError("Task.IsValid", "reporter_id", "invalid reporter")
	}

	return nil
}

// BeforeDB
func (t *Task) BeforeDB() {
	t.Status = convertStatus(t.StatusStr).(int64)
}

func statusFromInt(s int64) string {
	switch s {
	case TaskInProgress:
		return "in_progress"
	case TaskDone:
		return "done"
	default:
		return "backlog"
	}
}

func statusFromString(s string) int64 {
	switch s {
	case "in_progress":
		return TaskInProgress
	case "done":
		return TaskDone
	default:
		return TaskBacklog
	}
}

func convertStatus(status interface{}) interface{} {
	switch status := status.(type) {
	case string:
		return statusFromString(status)
	case int64:
		return statusFromInt(status)
	}
	return nil
}

func (t *Task) Populate(db *sqlx.DB) {
	if t.ReporterId != 0 {
		t.Reporter = *user.GetOne(t.ReporterId, db)
	} else {
		t.Worker = user.User{}
	}
	if t.WorkerId != 0 {
		t.Worker = *user.GetOne(t.WorkerId, db)
	} else {
		t.Worker = user.User{}
	}
	t.StatusStr = convertStatus(t.Status).(string)
}

// ToJson serializes the bot patch to json.
func (t *Task) ToJson() []byte {
	data, err := json.Marshal(t)
	if err != nil {
		return nil
	}

	return data
}

// BotPatchFromJson deserializes a bot patch from json.
func TaskFromJson(data io.Reader) *Task {
	decoder := json.NewDecoder(data)
	var taskData Task
	err := decoder.Decode(&taskData)
	if err != nil {
		return nil
	}

	return &taskData
}

// ToJson serializes the bot patch to json.
func (tl *List) ToJson() []byte {
	data, err := json.Marshal(tl)
	if err != nil {
		return nil
	}

	return data
}

// BotPatchFromJson deserializes a bot patch from json.
func TaskListFromJson(data io.Reader) *List {
	decoder := json.NewDecoder(data)
	var taskList List
	err := decoder.Decode(&taskList)
	if err != nil {
		return nil
	}

	return &taskList
}
